# Ansible Collection - redhatautomation.site_config

## Installation and Usage

### Installing the Collection from Automation Hub

Before using the site_config collection, you need to install it.
You can also include it in a `requirements.yml` file and install it via `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: https://gitlab.com/redhatautomation/site_config.git
    type: git
```

### Required Definitions:
```yaml
dmvpn_site: <number>
dmvpn_hub_ip: <ip address>
dmvpn_hub_preshared_key: <pre-shared key>
```

### DMVPN Hub must already be configured to accept spoke connections

### Using the collection
```yaml
---
- name: Configure Site(s)
  hosts: all
  gather_facts: no

  tasks:
    - name: Connect sites to DMVPN mesh
      include_role:
        name: redhatautomation.site_config.dmvpn_config

    - name: Configure network device(s)
      include_role:
        name: redhatautomation.site_config.banner_motd
```
